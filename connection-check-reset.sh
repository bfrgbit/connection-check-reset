#!/usr/bin/env bash

# this interface will be reset if connection is down
RESET_IF='wlan0'

# hostname used in connection test
TEST_HOST='google.com'

timestamp() {
    date +'%s'
}

last_test='n'
recent_failure=0

check_connection() {
    if ping4 -c 1 "${TEST_HOST}" 2>&1 >/dev/null; then # test exit code of `ping`
        if [[ "${last_test}" == 'n' ]]; then
            echo "$(timestamp) - Connection test succeeded"
        fi
        last_test='y'
        recent_failure=0
    else
        echo "$(timestamp) - Connection test failed"
        last_test='n'
        recent_failure=$((recent_failure + 1))
        ip link set "${RESET_IF}" down
        ip link set "${RESET_IF}" up
        echo "$(timestamp) - Reset interface: ${RESET_IF}"
        if [[ ${recent_failure} -ge 4 ]]; then
            echo "$(timestamp) - Connection test failed ${recent_failure} times. Waiting for 60 minutes..."
            sleep 58m
        elif [[ ${recent_failure} -ge 3 ]]; then
            echo "$(timestamp) - Connection test failed ${recent_failure} times. Waiting for 20 minutes..."
            sleep 18m
        elif [[ ${recent_failure} -ge 2 ]]; then
            echo "$(timestamp) - Connection test failed ${recent_failure} times. Waiting for 6 minutes..."
            sleep 4m
        else
            echo "$(timestamp) - Waiting for 2 minutes..."
        fi
    fi
}

check_connection
while sleep 2m; do
    check_connection
done
