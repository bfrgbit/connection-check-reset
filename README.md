# Linux connection check and interface reset daemon

This tiny service watches network connection based on the exit code of `ping4` and resets a designated network interface if the connection is lost. It is used to mitigate a well-known, long-lasting issue with several wireless network adapter drivers, which causes the wireless connection to drop after a certain amount of time.

References to the issue:

- <https://askubuntu.com/questions/694167/wifi-gets-disconnected-automatically>
- <https://askubuntu.com/questions/1030653/wifi-randomly-disconnected-on-ubuntu-18-04-lts>
- <https://askubuntu.com/questions/1032703/wifi-frequently-disconnecting-timeout>
- <https://unix.stackexchange.com/questions/525272/ubuntu-18-04-keep-disconnecting-from-home-wifi-virgin-media-but-keep-well-at>
- <https://help.ubuntu.com/stable/ubuntu-help/net-wireless-disconnecting.html.en>

While the solutions in the answers do not work for me on my Ubuntu 20.04 LTS laptop, I decided to mitigate the issue instead of trying to solve or workaround it. I noticed that when the issue happens on my laptop, restarting the `NetworkManager` service resets the state of the wireless interface and brings back the connection, thus this project.

Highlights:

- It uses a `ping` based method to detect connection failure. The target host is configurable, which could be an Internet destination (e.g., Google) or a local network destination (e.g., the gateway). If the failure is on DNS, an `inet addr` based method may not be able to detect the failure. The interface might have a valid IP address while the machine is not able to conduct most of the online activities.
- It resets the interface using `ip link set` commands, instead of `ifup` and `ifdown`, which no longer works on the latest version of `systemd` based Linux distributions.
- It registers a `systemd` service instead of using `crontab` to schedule script execution. This enabled dynamic retry timeout. This helps reduce interface reset attempts, specifically when the network connection failure is external.
- It provides logs to help you understand how often your connection fails.

References:

- <https://askubuntu.com/questions/4037/automatically-reconnect-wireless-connection>
- <https://askubuntu.com/questions/958697/is-there-a-better-way-to-automatically-check-and-restart-network-interfaces>
- <https://unix.stackexchange.com/questions/133931/automatically-restarting-network-connection>
- <https://linuxhint.com/restarting_network_ubuntu/>

## Usage

1. Fill in `RESET_IF`, i.e., the interface that you would like to reset in the case of connection failure. Usually, this is the wireless network interface that is responsible to establish the desired connection. Also, if necessary, replace `TEST_HOST` if you want to use a different well-known website to test an Internet connection, or if you intend to test a special network connection other than the Internet.
2. Put the Bash script in `/usr/local/bin`. If you put it elsewhere, you will need to modify the service file accordingly.
3. Put the service file in `/etc/systemd/system`.
4. `sudo systemctl daemon-reload ; sudo systemctl enable connection-check-reset ; sudo systemctl start connection-check-reset`
